import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  // sendBombSignal(dieTogether);
  const mockCallback = jest.fn(() => 'O_o');
  sendBombSignal(mockCallback);
  expect(mockCallback).toBeCalledWith('O_o');
});
